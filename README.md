# Vediansoft's Fantastic Forms

## Installation

Local configuration, add folder (like: _packages) to laravel installation (sail issues with symlinking otherwise)

Add this to your root laravel composer.json file, so the `project` from where you wish to execute the package

There are three ways to load the plugin, for local dvelopment we're not autoloading it as a `package`. We're just autoloading the `namespace` with the folder

## 1. Add namespace and folder to local installation
  
```json
"autoload": {
    "psr-4": {
        // "App\\": "app/",
        // "Database\\Factories\\": "database/factories/",
        // "Database\\Seeders\\": "database/seeders/",
        "Vediansoft\\Forms\\": "_formbuilder/"
    }
}
```

> this has to be added to the `composer.json` file in your `root laravel project` also comments above are to clarify which namespace + folder too add

## Load as a composer package (Local)

> this has to be added to the `composer.json` file in your `root laravel project`

```json
"repositories": [
    {
        "type": "path",
        "url": "_packages/*",
        "options": {
            "versions": {
                "vediansoft/*": "*"
            }
        }
    }
],
```

## Load as a composer package (Version Control System)

Development configuration (requires PAT_TOKEN)

> this has to be added to the `composer.json` file in your `root laravel project`

```json
"repositories": [
    {
        "type": "vcs",
        "url": "git@gitlab.com:vedian-soft/laravel/formbuilder.git"
    }
],
```

## Install package with composer if chosing for local package

> `skip` this step if you are only loading the folder and namespace

```bash
composer require vediansoft/laravel-formbuilder
```
