<?php

namespace Vediansoft\FantasticForms\Class;

use Illuminate\Database\Eloquent\Model;
use Vediansoft\FantasticForms\Helpers\BuilderCalls;
use Vediansoft\FantasticForms\Tools\ModelFieldsConstructor;

abstract class Builder
{
    use BuilderCalls;

    protected mixed $property;
    protected Builder $instanceBuilder;
    protected PropertyBuilder $propertyBuilder;
    protected string $currentName = '';
    protected array $currentArgument = [];

    // TODO: Move this to actual enum 
    protected const TYPES = [
        'fields' => [
            Model::class => ModelFieldsConstructor::class
        ]
    ];

    // TODO: Replace with Enum typing system
    protected function instanceException()
    {
        $instance = $this::class;
        $property = $this->property()->get();

        throw new \Exception("{$instance}::\${$property} does not exists");
    }

    // TODO: Replace with Enum typing system
    protected function property()
    {
        return $this->propertyBuilder;
    }

    // TODO: Replace with Enum typing system
    protected function setPropertyBuilder()
    {
        $this->propertyBuilder = new PropertyBuilder($this);
    }

    // TODO: Replace with Enum typing system
    protected function getInstanceProperty()
    {
        return $this->{$this->property()->get()} ?? $this;
    }

    // TODO: Replace with Enum typing system
    protected function setInstanceProperty()
    {
        $this->{$this->property()->get()} = $this->property()->getArgument();
    }

    // TODO: handle in propertyBuilder not here
    protected function isInstanceSetter()
    {
        return
            !empty($this->property()->getArgument())
            && !$this->isInstanceBuilder()
            && ($this->property()->isSet() || $this->property()->isChainable());
    }

    // TODO: Replace with Enum typing system
    protected function isInstanceBuilder()
    {
        return $this->property()->isChainable() && $this->instanceTypeExists();
    }

    // TODO: Replace with Enum typing system
    protected function buildInstance()
    {
        // Build instance
        switch ($this->property()->get()) {
            case 'fields';
                // TODO: Creat new builder calsses for each types
                // Use enum for
                $this->setInstanceBuilder('fields');
                break;
        }
    }

    // TODO: Replace with Enum typing system
    protected function unsetBuilders()
    {
        unset($this->propertyBuilder);
        unset($this->instanceBuilder);
        unset($this->currentArgument);
        unset($this->currentName);
    }

    protected function setCurrentName($name)
    {
        $this->currentName = $name;
    }

    // TODO: Replace with Enum typing system
    public function getCurrentName()
    {
        return $this->currentName;
    }

    // TODO: Replace with Enum typing system
    protected function setCurrentArgument($argument)
    {
        $this->currentArgument = $argument;
    }

    // TODO: Replace with Enum typing system
    public function getCurrentArgument()
    {
        return $this->currentArgument;
    }

    // TODO: Replace with Enum typing system
    protected function getInstanceType()
    {
        return self::TYPES[$this->property()->get()] ?? null;
    }

    // TODO: Replace with Enum typing system
    protected function isBuilderFor($class)
    {
        return !empty($this->getInstanceType()[$class] ?? null);
    }

    // TODO: Replace with Enum typing system
    protected function instanceTypeExists()
    {
        return !empty(self::TYPES[$this->property()->get()]);
    }

    // TODO: Replace with Enum typing system
    protected function hasInstanceProperty()
    {
        return property_exists($this, $this->property()->get());
    }

    // TODO: Replace with Enum typing system
    protected function setInstanceBuilder(): void
    {
        if ($this->isBuilderFor(Model::class)) {
            $this->setModelInstanceBuilder();
        }
    }

    private function setModelInstanceBuilder()
    {
        $this->instanceBuilder = ModelFieldsConstructor::setInstance($this)
            ->setModel($this->getEntity())
            ->build();
    }
}
