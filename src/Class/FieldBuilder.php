<?php

namespace Vediansoft\FantasticForms\Class;

use Vediansoft\FantasticForms\Contracts\FieldsContract;
use Vediansoft\FantasticForms\Helpers\FieldBuilderCalls;

abstract class FieldBuilder extends Builder implements FieldsContract
{
    use FieldBuilderCalls;

    protected $fields = [];

    protected $htmlAttributes = [
        'id' => '',
        'name' => '',
        'class' => '',
        'label' => '',
        'type'  => '',
        'value' => ''
    ];

    abstract function construct();

    public function build()
    {
        return $this->instance->setFields($this->fields);
    }
}
