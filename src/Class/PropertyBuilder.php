<?php

namespace Vediansoft\FantasticForms\Class;

use Illuminate\Support\Stringable;
use Vediansoft\FantasticForms\Types\PropertyType;
use Vediansoft\FantasticForms\Helpers\PropertyArguments;

class PropertyBuilder
{
    use PropertyArguments;

    protected Stringable $name;

    protected string $property = '';
    protected ?PropertyType $propertyType;

    protected mixed $argument = null;

    public function __construct(
        protected readonly Builder $instance
    ) {
        $this->setName();
        $this->setProperty();

        if (!empty($instance->getCurrentArgument())) {
            $this->setArgument();
        }
    }

    public function setName()
    {
        $this->name = new Stringable($this->instance->getCurrentName());
    }

    public function setProperty()
    {
        $this->property = $this->name
            ->replace('get', '')
            ->replace('set', '')
            ->lcfirst()
            ->toString();
    }

    public function getPropertyType()
    {
        return PropertyType::make($this->property);
    }

    public function get(?string $type = null)
    {
        return $this->{$type} ?? $this->property;
    }

    public function isSet()
    {
        return $this->get('name')
            ->startsWith('set');
    }

    public function isGet()
    {
        return $this->get('name')
            ->startsWith('get');
    }

    public function isChainable()
    {
        return !$this->isSet() && !$this->isGet();
    }
}
