<?php

namespace Vediansoft\FantasticForms\Class;

use Illuminate\Database\Eloquent\Model;
use Vediansoft\FantasticForms\Helpers\FormBuilderCalls;

class FormBuilder extends Builder
{
    use FormBuilderCalls;

    protected ?string $route;
    protected ?string $method;
    protected ?Model $entity;
    protected ?array $relations;
    protected ?array $with;
    protected ?array $fields;
    // protected ?FormRequest $rules;
}
