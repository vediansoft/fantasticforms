<?php

namespace Vediansoft\FantasticForms\Helpers;

trait BuilderCalls
{
    public function __call($name, $arguments)
    {
        $this->setCurrentName($name);
        $this->setCurrentArgument($arguments);
        $this->setPropertyBuilder();

        switch (true) {
            case $this->isInstanceSetter():
                $this->setInstanceProperty();
                break;
            case $this->isInstanceBuilder():
                $this->buildInstance();
                break;
            case !$this->hasInstanceProperty():
                $this->instanceException();
                break;
            default:
                return $this->getInstanceProperty();
        }

        $this->unsetBuilders();

        return $this;
    }
}
