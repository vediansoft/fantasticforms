<?php

namespace Vediansoft\FantasticForms\Helpers;

use Illuminate\Database\Eloquent\Model;
use Vediansoft\FantasticForms\Types\PropertyType;

trait PropertyArguments
{
    public function getArgument()
    {
        return $this->argument;
    }

    public function hasMultipleArguments()
    {
        return is_array($this->instance->getCurrentArgument()) && count($this->instance->getCurrentArgument()) > 1;
    }

    private function determineArgumentWeight()
    {
        $this->argument = $this->hasMultipleArguments() ?
            $this->instance->getCurrentArgument() :
            $this->instance->getCurrentArgument()[0];

        return match ($this->getPropertyType()) {
            PropertyType::ROUTE => $this->getRouteArgument(),
            PropertyType::DEFAULT => $this->getArgument(),
        };
    }

    private function setArgument()
    {
        $this->argument = $this->determineArgumentWeight();
    }

    private function routeBinding()
    {
        return $this->modelBinding()
            ? [$this->getArgument(), $this->instance->getEntity()]
            : $this->getArgument() ?? ['', []];
    }

    private function modelBinding()
    {
        return !$this->hasMultipleArguments()
            && $this->instance->getEntity() instanceof Model;
    }

    private function getRouteArgument()
    {
        return route($this->routeBinding()[0], $this->routeBinding()[1]);
    }
}
