<?php

namespace Vediansoft\FantasticForms\Helpers;

use Illuminate\Support\Str;

class PropHelper
{
    public function __invoke(
        string $name,
        string $pattern = ''
    ) {
        return Str::of($name)
            ->replaceMatches($pattern, '')
            ->lower()
            ->toString();
    }
}
