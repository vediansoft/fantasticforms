<?php

namespace Vediansoft\FantasticForms\Helpers;


trait FieldBuilderCalls
{
    private array $attributes = [];

    public function __invoke($name, $arguments)
    {
        $this->rename($name);
        $this->{$name} = $arguments[0];

        return $this;
    }

    public function __get($name)
    {
        return $this->attributes[$name];
    }

    public function __set($name, $arg)
    {
        $this->attributes[$name] = is_array($arg) ? collect($arg) : $arg;
    }

    public static function __callStatic($name, $arguments)
    {
        return (new static)($name, $arguments);
    }

    private function rename(&$name)
    {
        $name = (new PropHelper)($name, "/(get|set)/");
    }
}
