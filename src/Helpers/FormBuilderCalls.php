<?php

namespace Vediansoft\FantasticForms\Helpers;

trait FormBuilderCalls
{
    public static function __callStatic($name, $arguments)
    {
        return (new static)->{$name}($arguments[0] ?? $arguments);
    }
}
