<?php

namespace Vediansoft\FantasticForms\Tools;

use Vediansoft\FantasticForms\Class\FieldBuilder;
use Vediansoft\FantasticForms\Contracts\ModelFieldsContract;

class ModelFieldsConstructor extends FieldBuilder implements ModelFieldsContract
{
    private function determineFieldValue($arg)
    {
        // TODO:: Use Enum/Type instances for determining types
        if ($this->determineFieldType($arg) !== 'password') {
            $value = $this->model?->$arg ?? null;
        }

        return $value ?? null;
    }
    private function determineFieldType($arg)
    {
        /**
         * @todo
         * ====================================================================
         * This FieldTypes needs to be build once we start reorganising classes below:
         * - @var namespace Vediansoft\FantasticForms\Helpers\BuilderCalls::class
         * - @var Vediansoft\FantasticForms\Builder\Builder::class
         * ====================================================================
         */

        if ($this->getMasked()->contains($arg)) {
            return 'password';
        } elseif ($this->fillable->contains('email')) {
            return 'email';
        }

        return 'text';
    }

    protected function getMasked()
    {
        return $this->hidden->reject(function ($arg) {
            return !$this->fillable->contains($arg);
        });
    }

    private function map(?callable $callback = null)
    {
        if ($callback)
            return $this->fillable->map($callback);
        else
            return $this->fillable;
    }

    public function construct()
    {
        return $this->map(function ($arg) {
            return [
                'name'  => $arg,
                'type'  => $this->determineFieldType($arg),
                'value' => $this->determineFieldValue($arg)
            ];
        })->toArray();
    }

    public function build()
    {
        $this->hidden   = $this->model->getHidden();
        $this->visible  = $this->model->getVisible();
        $this->fillable = $this->model->getFillable();
        $this->guarded  = $this->model->getGuarded();
        $this->fields   = $this->construct();

        return parent::build();
    }
}
