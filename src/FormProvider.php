<?php

namespace Vediansoft\FantasticForms;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Vediansoft\FantasticForms\Class\FormBuilder;

class FormProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        App::bind('form', function () {
            return new FormBuilder;
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
    }
}
