<?php

namespace Vediansoft\FantasticForms\Contracts;

interface FieldsContract
{
    public function build();
}
