<?php

namespace Vediansoft\FantasticForms\Types;

/**
 * @deprecated
 * ====================================================================
 * This type needs to be build once we start reorganising classes below:
 * - @var namespace Vediansoft\FantasticForms\Helpers\BuilderCalls::class
 * - @var Vediansoft\FantasticForms\Builder\Builder::class
 * ====================================================================
 */
enum MethodType: string
{
    const SET = 'set';
    const GET = 'get';
    const IS  = 'is';
    const NOT = 'not';

    case HAS    = MethodType::IS;
    case HASNT  = MethodType::NOT;

    case SETTER = MethodType::SET;
    case GETTER = MethodType::GET;
}
