<?php

namespace Vediansoft\FantasticForms\Types;

enum PropertyType: string
{
    case DEFAULT = 'default';
    case ROUTE = 'route';

    public static function make($property)
    {
        return match (self::tryFrom($property)) {
            null => PropertyType::DEFAULT,
            PropertyType::ROUTE => PropertyType::ROUTE,
        };
    }
}
